/* 
 * EuclideanTransform3D: A class to represent an Euclidean transform in 3D
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _ETRANS3D_H_
#define _ETRANS3D_H_

#include "mkecc/json.hpp"
#include "mkecc/geom/itrans.h"

namespace mke {
namespace cc {
namespace geom {
    
/** \addtogroup mkecc
 *  @{
 */
      
class EuclideanTransform3D : public InvertibleTransformation {
public:

  // Construction ============================================================  
  
  EuclideanTransform3D();  
  EuclideanTransform3D(const EuclideanTransform3D &trans);
  ~EuclideanTransform3D();
  
  EuclideanTransform3D& operator=(const EuclideanTransform3D& trans);
  EuclideanTransform3D& operator=(EuclideanTransform3D &&trans) noexcept;
  
  void get(nlohmann::json &trans) const;
  void set(const nlohmann::json &trans);
  
  void validateDynamic(const nlohmann::json &trans) const;
  const nlohmann::json& getSchemaDynamic(void) const;  
  static void validate(const nlohmann::json &trans);
  static const nlohmann::json& getSchema(void);
  
  void invert(void);  
  bool isIdentity(void);
  
  void transformPoints(const double* const u, double* const v, const size_t no_pts) const;
  void inverseTransformPoints(const double* const u, double* const v, const size_t no_pts) const;
  
  struct Impl;  // Forward declaration of the implementation class
private:
  Impl *impl_;  // PIMPL  
};

/** @}*/

} // namespace geom
} // namespace cc
} // namespace mke

#endif // _ETRANS3D_H_
