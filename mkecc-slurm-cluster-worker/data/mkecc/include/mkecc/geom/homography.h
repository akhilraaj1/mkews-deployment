/* 
 * Homography: A class to represent 2D homography
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _HOMOGRAPHY_H_
#define _HOMOGRAPHY_H_

#include "mkecc/json.hpp"
#include "mkecc/geom/itrans.h"

namespace mke {
namespace cc {
namespace geom {
    
/** \addtogroup mkecc
 *  @{
 */
      
class Homography : public InvertibleTransformation {
public:

  // Construction ============================================================  
  
  Homography();  
  Homography(const Homography &trans);
  ~Homography();
  
  Homography& operator=(const Homography& trans);
  Homography& operator=(Homography &&trans) noexcept;
  
  void get(nlohmann::json &trans) const;
  void set(const nlohmann::json &trans);
  
  void validateDynamic(const nlohmann::json &trans) const;
  const nlohmann::json& getSchemaDynamic(void) const;  
  static void validate(const nlohmann::json &trans);
  static const nlohmann::json& getSchema(void);

  void invert(void);  
  void normalize(void);
  
  void transformPoints(const double* const u, double* const v, const size_t no_pts) const;
  void inverseTransformPoints(const double* const u, double* const v, const size_t no_pts) const;
  void recover(const double* const u, const double* const v, const size_t no_points);

  struct Impl;  // Forward declaration of the implementation class
private:
  Impl *impl_;  // PIMPL  
};

/** @}*/

} // namespace geom
} // namespace cc
} // namespace mke

#endif // _HOMOGRAPHY_H_
