/* Info - utility functions for various info strings
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _MKECC_INFO_H_
#define _MKECC_INFO_H_

#include <string>

namespace mke {
namespace cc {
namespace util {

/** \addtogroup mkecc
 *  @{
 */

std::string GetTimestampString(void);

std::string GetSystemInfoString(void);

std::string GetHostnameString(void);

std::string GetUsernameString(void);

std::string GetVersionString(void);

std::string GetInfoString(void);

/** @}*/

} // namespace util
} // namespace cc
} // namespace mke

#endif // _MKECC_INFO_H_
