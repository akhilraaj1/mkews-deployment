/*
 * Exception
 *
 * Copyright (c) 2020, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _MKECC_EXCEPTION_H_
#define _MKECC_EXCEPTION_H_

#include <stdexcept>

namespace mke {
namespace cc {
namespace util {

/** \addtogroup mkedd
 *  @{
 */

struct exception : public virtual std::runtime_error
{
private:
  std::string msg_;

public:
  exception(const std::string &arg, const char *file, int line);
  exception(const std::string &arg);
  ~exception() throw();

  const char *what() const throw();
};

#ifdef WITH_DEBUG_EXCEPTIONS
#define throw_ccexception(arg) throw mke::cc::util::exception(arg, __FILE__, __LINE__)
#else
#define throw_ccexception(arg) throw mke::cc::util::exception(arg)
#endif


#ifdef WITH_DEBUG_EXCEPTIONS
#define make_ccexception_ptr(arg) std::make_exception_ptr(mke::cc::util::exception(arg, __FILE__, __LINE__))
#else
#define make_ccexception_ptr(arg) std::make_exception_ptr(mke::cc::util::exception(arg))
#endif


#define MKECC_DEBUGLINE std::cout << __FILE__ << ":" << __LINE__ << std::endl;
#define MKECC_DEBUGLINE_PARAMS(p) std::cout << __FILE__ << ":" << __LINE__ << \
                                  ": " << p << std::endl;

/** @}*/

} /* namespace util */
} /* namespace cc */
} /* namespace mke */


#endif // _MKECC_EXCEPTION_H_
