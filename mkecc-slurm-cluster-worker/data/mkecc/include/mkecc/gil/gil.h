/* 
 * gil.h: Image data interfacing and algorithms using boost::gil
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _GIL_H_
#define _GIL_H_

// Get rid off boost::gil warnings
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"

#include <boost/version.hpp>
#if (BOOST_VERSION < 106800)
  #include <boost/gil/gil_all.hpp>
  #include <boost/gil/extension/dynamic_image/dynamic_image_all.hpp>
#else
  #include <boost/gil.hpp>
  #include <boost/gil/extension/dynamic_image/dynamic_image_all.hpp>
#endif

namespace mke {
namespace cc {
namespace gil {

typedef boost::mpl::vector<boost::gil::gray8_image_t, boost::gil::gray16_image_t> ImageTypes;
typedef boost::gil::any_image<ImageTypes> Image;
typedef Image::view_t ImageView;
typedef Image::const_view_t ConstImageView;

template <typename Pixel>
boost::gil::image<Pixel> * copy_image(const ImageView & iview)
{
  boost::gil::image<Pixel> * im = new boost::gil::image<Pixel>(iview.width(), iview.height());
  boost::gil::copy_pixels(iview, boost::gil::view(*im));
  
  return im;
}

#if (BOOST_VERSION >= 106800)
template <typename T, typename... Ts>
bool holds_alternative(const boost::variant<Ts...>& v) noexcept
{
  return boost::get<T>(&v) != nullptr;
}
#endif

template <typename T>
bool current_view_type_is(const mke::cc::gil::ImageView &view)
{
#if (BOOST_VERSION < 107000)
  return view.current_type_is<T>();
#else
  return holds_alternative<T>(view);
#endif
}

template<typename T>
T get_view(const mke::cc::gil::ImageView &view)
{
#if (BOOST_VERSION < 107000)
  return view._dynamic_cast<T>();
#else
  return boost::get<T>(view);
#endif
}


#ifdef cimg_version
// Convenience function for CImg library image

inline ImageView GetImageView(const cimg_library::CImg<unsigned char> &img)
{
  size_t w = img.width();
  size_t h = img.height();

  return mke::cc::gil::ImageView(boost::gil::interleaved_view(w, h,
                                        (boost::gil::gray8_pixel_t*) img.data(), w));
}

#endif

} // namespace gil
} // namespace cc
} // namespace mke

#endif

