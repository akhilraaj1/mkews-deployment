/* 
 * Model: Camera model
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _MODEL_H_
#define _MODEL_H_

#include <vector>
#include "mkecc/json.hpp"

namespace mke {
namespace cc {
namespace camera {

enum ModelType {
    UNKNOWN_MODEL = 0,
    OPENCV_MODEL = 1,
    CUBIC_MODEL = 2,
    D3P_FISHEYE_MODEL = 3,
    P8P_FISHEYE_MODEL = 4,
    E7P_FISHEYE_MODEL = 5
};
    
/** \addtogroup mkecc
 *  @{
 */
      
class Model {
public:

  // Construction ============================================================  
  
  Model();  
  Model(const Model &model);
  ~Model();
  
  Model& operator=(const Model& model);
  Model& operator=(Model &&model) noexcept;
  
  void get(nlohmann::json &model) const;
  void set(const nlohmann::json &model);
  static void validate(const nlohmann::json &model);
  static const nlohmann::json& getSchema(void);

  ModelType getType(void) const;
  void setType(const std::string &type_str);
  void setType(const ModelType type);

  size_t getWidth() const;
  void setWidth(size_t width);
  size_t getHeight() const;
  void setHeight(size_t height);

  const std::vector<bool>& getOptFlags(void) const;
  void setOptFlags(const std::vector<bool> &opt);
  void setOptFlags(const nlohmann::json &opt);
  void setOptFlags(bool opt);
  void clearDist(void);
  
  bool isFishEye(void);
  static bool isFishEye(const ModelType type);

  struct Impl;  // Forward declaration of the implementation class
protected:
  Impl *impl_;  // PIMPL  
};

/** @}*/

} // namespace camera
} // namespace cc
} // namespace mke

#endif // _MODEL_H_
