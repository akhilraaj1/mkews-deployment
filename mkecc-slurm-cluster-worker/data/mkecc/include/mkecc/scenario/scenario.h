/* 
 * Scenario 
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _SCENARIO_H_
#define _SCENARIO_H_

#include <memory>
#include <string>

#include "mkecc/json.hpp"

namespace mke {
namespace cc {
namespace scenario {

/** \addtogroup mkecc
 *  @{
 */

class Scenario {
  public:

  // Construction =============================================================
      
  Scenario();
  Scenario(const Scenario &) = delete;
  Scenario(Scenario&&) = default;
  ~Scenario();
  
  Scenario& operator=(const Scenario&) = delete;
  Scenario& operator=(Scenario &&d) = default;

  // Description ==============================================================

  const nlohmann::json& getDescription(void) const;
  void setDescription(const nlohmann::json &desc);
  void validateDescription(const nlohmann::json &desc) const;

  // Schema ===================================================================

  const nlohmann::json& getSchema(void) const;
  static const nlohmann::json& getSchemaStatic(void);

  // Scenario data ============================================================

  void setData(const nlohmann::json &data);
  const nlohmann::json& getData(void) const;
  nlohmann::json findData(const std::string &key) const;
  void swapData(nlohmann::json &swap_data);

  // Scenario parameters ======================================================

  void setParams(const nlohmann::json &params);
  nlohmann::json getParams(void) const;
  nlohmann::json getParamsSchema(void) const;

  // Nodes ====================================================================

  void setNodeParams(const std::string &node_id, const nlohmann::json &params);
  nlohmann::json getNodeParams(const std::string &node_id) const;
  nlohmann::json getNodeParamsSchema(const std::string &node_id) const;
  bool hasNode(const std::string &node_id) const;

  static nlohmann::json getNodeTypeParamsSchema(const std::string &node_type);
  static nlohmann::json getAvailableNodes(void);

  // Datasets =================================================================

  void getDatasetsDescription(nlohmann::json &out_desc) const;
  void setDatasets(const nlohmann::json &dts);
  const nlohmann::json& getDatasets(void);
  void swapDatasets(nlohmann::json &swap_dts);

  // Execution ================================================================

  void execute(void);
  void execute(const nlohmann::json &ids); 
  void executeIfEmpty(const std::string &id);
  
private:
  class Impl;  // Forward declaration of the implementation class
  Impl * impl_; // PIMPL
};

/** @}*/
    
} // namespace scenario
} // namespace cc
} // namespace mke


#endif // _SCENARIO_H_
