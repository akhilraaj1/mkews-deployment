/* 
 * Factory: factory for instantiating scenario node objects
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _NODE_FACTORY_H
#define _NODE_FACTORY_H

#include <map>
#include "mkecc/scenario/node.h"
#include "mkecc/json.hpp"

namespace mke {
namespace cc {
namespace scenario {
    
/** \addtogroup mkecc
 *  @{
 */
    
class Factory {
public:    
  static Node* create(const std::string &name);
  static Node* create(const NodeType type);
    
  static nlohmann::json getAvailableNodes(void); 
  
private:
  const static std::map<const std::string, Node*(*)(void)> create_map_;
  const static std::map<NodeType, Node*(*)(void)> type_map_;
};    


/** @}*/


} // namespace scenario
} // namespace cc
} // namespace mke

#endif // _NODE_FACTORY_H
