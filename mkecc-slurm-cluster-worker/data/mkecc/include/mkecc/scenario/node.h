/* 
 * Node: Base class for all scenario nodes
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _NODE_H_
#define _NODE_H_

#include <vector>

#include "mkecc/json.hpp"
#include "mkecc/util/pmap.h"
#include "mkecc/util/shared_json.h"
#include "mkecc/util/exception.h"

namespace mke {
namespace cc {
namespace scenario {

/** \addtogroup mkecc
 *  @{
 */

typedef enum { 
  UNKNOWN_NODE = 0,
  CAMERA_CALIBRATION_NODE = 1,
  CAMERA_POSE_NODE = 2,
  TARGET_POINTS_NODE = 3,
  UNCONSTRAINED_DOE_NODE = 4,
  DOE_GROUPS_MERGE_NODE = 5,
  RAY_SAMPLER_NODE = 6,
  MKEDET_EXPORTER_NODE = 7,
  EXTERNAL_DATA_NODE = 8,
  ID_TARGET_POINTS_NODE = 9,
  NONCENTRAL_DOE_NODE = 10,
  CONSTRAINED_DOE_NODE = 11,
  VCSEL_TOPOLOGY_NODE = 12,
  RAY_TOPOLOGY_NODE = 13,
  SLIDER_CALIBRATION_NODE = 14,
  FRAME_CORRECTION_NODE = 15
} NodeType;    
      
class Node {
public:

  // Construction ============================================================  
  
  Node();
  virtual ~Node();
  
  // Assignment operators =====================================================
  
  Node& operator=(const Node& node) = delete;
  Node& operator=(Node &&node) noexcept;
  
  // Base methods =============================================================
  
  void setId(const std::string &id);
  const std::string & getId(void) const;
  void addParent(Node *parent);
  const std::vector<Node*>& getParents(void) const;
  
  void setScenarioData(mke::cc::util::shared_json *data);
  void setScenarioParams(mke::cc::util::ParameterMap *spmap);
  void setDatasets(mke::cc::util::shared_json *dts);

  void setParams(const nlohmann::json &params);
  nlohmann::json getParams(void) const;
  nlohmann::json getParamsSchema(void) const;
  
  // Virtual methods ==========================================================
  
  virtual const std::string& getName(void) const = 0;
  virtual NodeType getType(void) const = 0;

  virtual void validateParams(const nlohmann::json &params) const = 0;
  virtual void execute(void) = 0;
  virtual void getDatasetsDescription(nlohmann::json &desc) const = 0;
  
  // Implementation ===========================================================
  
protected:
  struct Impl;  // Forward declaration of the implementation class
  Impl *impl_; // PIMPL  
};

/** @}*/

} // namespace scenario
} // namespace cc
} // namespace mke

#endif
