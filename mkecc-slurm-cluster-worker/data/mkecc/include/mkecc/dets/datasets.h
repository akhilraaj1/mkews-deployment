/* 
 * Datasets 
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _DATASETS_H_
#define _DATASETS_H_

#include <memory>
#include "mkecc/json.hpp"
#include "mkecc/gil/gil.h"

namespace mke {
namespace cc {
namespace dets {

/** \addtogroup mkecc
 *  @{
 */

class Datasets {
  public:

  // Construction =============================================================

  Datasets();
  Datasets(const Datasets&) = delete;
  Datasets(Datasets&&) = default;
  ~Datasets();
  
  Datasets& operator=(const Datasets&) = delete;
  Datasets& operator=(Datasets &&) = default;

  // Description ==============================================================
  
  void setDescription(const nlohmann::json &desc);
  const nlohmann::json& getDescription(void) const;
  void validateDescription(const nlohmann::json &desc);

  // Schema ===================================================================

  const nlohmann::json& getSchema(void) const;
  const nlohmann::json& getImageListSchema(void) const;

  // Frame retention ==========================================================

  const std::string& getFrameRetentionRoot(void) const;
  void setFrameRetentionRoot(const std::string &froot);

  // Frame processing =========================================================

  void initialize(void);
  void processFrame(const mke::cc::gil::ImageView& iview, const std::string &dtsid);
  void processImageFile(const std::string & filepath, const std::string & dtsid);
  void processImageList(const nlohmann::json &image_list);
  void setNoThreads(size_t no_threads);
  size_t getNoThreads(void) const;
  void setMaxQueueSize(size_t max_queue_size);
  size_t getMaxQueueSize(void) const;

  // Datasets results =========================================================

  const nlohmann::json& getDatasets(void) const;
  void getAndClearDatasets(nlohmann::json &out_dts);
  
  // Helper functions to access datasets results ==============================
  
  static const nlohmann::json* getResultsByDetectorType(const nlohmann::json& dts, const size_t frame_idx, const std::string &type_str);
  static const nlohmann::json* getFrameByIdx(const nlohmann::json& dts, const size_t frame_idx);
  
private:
  class Impl;  // Forward declaration of the implementation class
  //std::unique_ptr<Impl> impl_; // PIMPL
  Impl * impl_; // PIMPL
};

/** @}*/
    
} // namespace dets
} // namespace cc
} // namespace mke


#endif // _DATASETS_H_
