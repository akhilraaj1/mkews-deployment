#----------------------------------------------------------------
# Generated CMake target import file for configuration "RELEASE".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "mkecc" for configuration "RELEASE"
set_property(TARGET mkecc APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(mkecc PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libmkecc.so.0.2.0"
  IMPORTED_SONAME_RELEASE "libmkecc.so.0.2.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS mkecc )
list(APPEND _IMPORT_CHECK_FILES_FOR_mkecc "${_IMPORT_PREFIX}/lib/libmkecc.so.0.2.0" )

# Import target "mkecc_static" for configuration "RELEASE"
set_property(TARGET mkecc_static APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(mkecc_static PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libmkecc.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS mkecc_static )
list(APPEND _IMPORT_CHECK_FILES_FOR_mkecc_static "${_IMPORT_PREFIX}/lib/libmkecc.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
