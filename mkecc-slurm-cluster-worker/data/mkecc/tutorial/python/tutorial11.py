import pymkecc
import json
import os

###############################################################################

desc = [
    {
        'id': 'ccalib',
        'type': 'CameraCalibration',
        'parameters': {
            'camera_model_type': 'OPENCV_MODEL',
            'target_type': 'ELMARK_CHESSBOARD_TARGET',
            'camera_model_opt': [True, True, True, True, True, True, True, True],
            'target_parameters': {
                'x_mm': 29.63,
                'y_mm': 29.63
            }
        }
    }
]

scenario = pymkecc.Scenario()
scenario.setDescription(desc)

###############################################################################

dts = pymkecc.Datasets()
dts_desc = scenario.getDatasetsDescription()
dts.setDescription(dts_desc)

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')

image_list = [
    {
        'dataset': 'ccalib',
        'directory': {
            'path': ccalib_path,
            'mask': '.*',
            'order': 'lex',
        }
    }
]

dts.initialize()
dts.processImageList(image_list)

dts_data = dts.getDatasets()
scenario.setDatasets(dts_data)

###############################################################################

scenario.executeAll()
data = scenario.getData()
camera_model = data['ccalib']['camera_model']

###############################################################################

print('Calibrated camera model:')
print(json.dumps(camera_model, indent=2) + '\n')
