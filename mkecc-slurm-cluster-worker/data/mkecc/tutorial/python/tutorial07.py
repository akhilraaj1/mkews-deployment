import pymkecc
import os
from PIL import Image
import matplotlib.pyplot as plt
import json

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')

###############################################################################

desc = [
    {
        'id': 'dts1',
        'detectors': [
            {
                'name': 'CHESS_DETECTOR',
                'parameters': {}
            }
        ]
    },
    {
        'id': 'dts2',
        'detectors': [
            {
                'name': 'CHESS_DETECTOR',
                'parameters': {}
            },
            {
                'name': 'ELLIPSE_DETECTOR',
                'parameters': {}
            }
        ]
    }
]

dts = pymkecc.Datasets()
dts.setDescription(desc)

###############################################################################

image_list = [
    {
        'dataset': 'dts1',
        'directory': {
            'path': ccalib_path,
            'mask': '.*',
            'order': 'lex',
        }
    },
    {
        'dataset': 'dts2',
        'directory': {
            'path': ccalib_path,
            'mask': '.*',
            'order': 'lex',
        }
    }
]

dts.initialize()
dts.processImageList(image_list)

###############################################################################

plt_type = ['ro', 'bo']
data = dts.getDatasets()
image_idx = 9

for dts in ['dts1', 'dts2']:
    img_path = data[dts]['frames'][image_idx]['uri'][7:]
    no_dets = len(data[dts]['detectors'])

    im = Image.open(img_path)
    plt.figure()
    plt.imshow(im)

    for i in range(no_dets):
        results = data[dts]['detectors'][i]['results'][image_idx]

        for p in results:
            plt.plot(p[0], p[1], plt_type[i])

plt.show()
