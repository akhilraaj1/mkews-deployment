import pymkecc
import json

###############################################################################

avail_dets = pymkecc.Detector.getAvailableDetectors()
print('Available detectors:')
print(avail_dets)
print()

###############################################################################

chess_det = pymkecc.Detector.create('CHESS_DETECTOR')
meta_info = chess_det.getMetaInfo()
print('CHESS_DETECTOR information:')
print(json.dumps(meta_info, indent=2))
print()

params = chess_det.getParams()
print('CHESS_DETECTOR parameters:')
print(json.dumps(params, indent=2))
print()

param_schema = chess_det.getParamSchema()
print('CHESS_DETECTOR parameter schema:')
print(json.dumps(param_schema, indent=2))
print()

###############################################################################

ell_det = pymkecc.Detector.create('ELLIPSE_DETECTOR')
meta_info = ell_det.getMetaInfo()
print('ELLIPSE_DETECTOR information:')
print(json.dumps(meta_info, indent=2))
print()

params = ell_det.getParams()
print('ELLIPSE_DETECTOR parameters:')
print(json.dumps(params, indent=2))
print()

param_schema = ell_det.getParamSchema()
print('ELLIPSE_DETECTOR parameter schema:')
print(json.dumps(param_schema, indent=2))
print()
