import pymkecc
import os
from PIL import Image
import numpy
import matplotlib.pyplot as plt

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')

img_file1 = os.path.join(ccalib_path, 'img_0000.png')
img_file2 = os.path.join(ccalib_path, 'img_0001.png')
img_file3 = os.path.join(ccalib_path, 'img_0002.png')

###############################################################################

desc = [
    {
        'id': 'dts1',
        'detectors': [
            {
                'name': 'CHESS_DETECTOR',
                'parameters': {}
            }
        ]
    },
    {
        'id': 'dts2',
        'detectors': [
            {
                'name': 'CHESS_DETECTOR',
                'parameters': {}
            },
            {
                'name': 'ELLIPSE_DETECTOR',
                'parameters': {}
            }
        ]
    }
]

dts = pymkecc.Datasets()
dts.setDescription(desc)

###############################################################################

dts.initialize()

im = Image.open(img_file1)
dts.processFrame(numpy.array(im), 'dts1')
dts.processImageFile(img_file2, 'dts1')
dts.processImageFile(img_file3, 'dts1')

dts.processImageFile(img_file1, 'dts2')
dts.processImageFile(img_file2, 'dts2')
dts.processImageFile(img_file3, 'dts2')

###############################################################################

data = dts.getDatasets()
frame_idx = 1
dot_det_idx = 0
ell_det_idx = 1

img_path = data['dts1']['frames'][frame_idx]['uri'][7:]
results = data['dts1']['detectors'][dot_det_idx]['results'][frame_idx]

im = Image.open(img_path)
plt.figure()
plt.imshow(im)

for p in results:
    plt.plot(p[0], p[1], 'ro')

###

img_path = data['dts2']['frames'][frame_idx]['uri'][7:]
results1 = data['dts2']['detectors'][dot_det_idx]['results'][frame_idx]
results2 = data['dts2']['detectors'][ell_det_idx]['results'][frame_idx]

im = Image.open(img_path)
plt.figure()
plt.imshow(im)

for p in results1:
    plt.plot(p[0], p[1], 'ro')

for p in results2:
    plt.plot(p[0], p[1], 'bo')

plt.show()

