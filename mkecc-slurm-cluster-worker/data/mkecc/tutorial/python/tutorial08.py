import pymkecc
import os
from PIL import Image
import numpy
import matplotlib.pyplot as plt

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')

img_file1 = os.path.join(ccalib_path, 'img_0000.png')
img_file2 = os.path.join(ccalib_path, 'img_0001.png')

###############################################################################

desc = [
    {
        'id': 'dts1',
        'detectors': [
            {
                'name': 'CHESS_DETECTOR',
                'parameters': {}
            },
            {
                'name': 'ELLIPSE_DETECTOR',
                'parameters': {}
            }
        ]
    }
]

dts = pymkecc.Datasets()
dts.setDescription(desc)

###############################################################################

dts.initialize()
dts.processImageFile(img_file1, 'dts1')
dts.processImageFile(img_file2, 'dts1')
data = dts.getDatasets()

###############################################################################

w = data['dts1']['frames'][0]['width']
h = data['dts1']['frames'][0]['height']

camera_model = {
    'type': 'OPENCV_MODEL',
    'width': w,
    'height': h,
    'k': [w, w, w/2, h/2],
    'dist': [0, 0, 0, 0, 0, 0, 0, 0],
    'opt': [True, True, True, True, True, True, True, True]
}


target = pymkecc.Target.create('ELMARK_CHESSBOARD_TARGET')
target.setCameraModel(camera_model)

target.initialize(data['dts1'], 0)
target.detect()

results = target.getResults()

###############################################################################

image_crds = results['image_coords']
target_crds = results['target_coords']

im = Image.open(img_file1)
plt.figure()
plt.imshow(im)

for i in range(len(image_crds)):
    u = image_crds[i]
    t = target_crds[i]

    plt.plot(u[0], u[1], 'ro')
    plt.text(u[0], u[1], '[%.2f, %.2f]' % (t[0], t[1]), fontsize=12, color='red')

plt.show()

