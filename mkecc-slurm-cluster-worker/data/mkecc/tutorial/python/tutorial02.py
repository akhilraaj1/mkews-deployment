import pymkecc
import os
from PIL import Image
import numpy
import matplotlib.pyplot as plt

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')
img_path = os.path.join(ccalib_path, 'img_0000.png')
im = Image.open(img_path)

###############################################################################

det = pymkecc.Detector.create('CHESS_DETECTOR')
results = det.process(numpy.array(im))

###############################################################################

plt.imshow(im)

for p in results:
    plt.plot(p[0], p[1], 'ro')

plt.show()
