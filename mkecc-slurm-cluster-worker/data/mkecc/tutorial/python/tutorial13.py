import pymkecc
import json
import os

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')
template_path = os.path.join(tutorial_dir, '..', '..', 'data', 'templates', 'camera_calib.json')

macros = {
    'DATA_PATH': ccalib_path
}

session_template = pymkecc.SessionTemplate()
session_template.setFromFile(template_path)
session = session_template.getInstance(macros)

###############################################################################

ilist = session_template.getImageList(macros)

session.initializeDatasets()
session.processImageList(ilist)
session.finalizeDatasets()

###############################################################################

session.executeAll()
data = session.getScenarioData()
camera_model = data['ccalib']['camera_model']

###############################################################################

print('Calibrated camera model:')
print(json.dumps(camera_model, indent=2) + '\n')
