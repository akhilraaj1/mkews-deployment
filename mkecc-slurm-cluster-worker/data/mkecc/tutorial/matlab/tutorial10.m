function tutorial10

nodes = MkeccScenario.getAvailableNodes();

fprintf('Available scenario nodes: \n');
disp(nodes);

%%%

schema = MkeccScenario.getNodeTypeParamsSchema('CameraCalibration');

fprintf('"CameraCalibration" parameter schema: \n');
MkeccJsonFile.Dump(schema);

%%%

node.id = 'ccalib';
node.type = 'CameraCalibration';
node.parameters = struct;
node.parameters.camera_model_type = 'OPENCV_MODEL';
node.parameters.target_type = 'ELMARK_CHESSBOARD_TARGET';

scenario_description = { node };

scenario = MkeccScenario();
scenario.setDescription(scenario_description);

%%%

dts_description = scenario.getDatasetsDescription();

fprintf('\nScenario induced datasets description: \n');
MkeccJsonFile.Dump(dts_description);

