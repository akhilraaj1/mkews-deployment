function tutorial11

node.id = 'ccalib';
node.type = 'CameraCalibration';
node.parameters = struct;
node.required = [];
node.parameters.camera_model_type = 'OPENCV_MODEL';
node.parameters.target_type = 'ELMARK_CHESSBOARD_TARGET';
node.parameters.camera_model_opt = [true, true, true, true, true, true, true, true];
node.parameters.target_parameters.x_mm = 29.63;
node.parameters.target_parameters.y_mm = 29.63;

scenario_description = { node };

scenario = MkeccScenario();
scenario.setDescription(scenario_description);

%%%

dts = MkeccDatasets();
dts_description = scenario.getDatasetsDescription();
dts.setDescription(dts_description);

%%%

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

ccalib_path = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', 'ccalib');

list1.dataset = 'ccalib';
list1.directory.path = ccalib_path;
list1.directory.mask = '.*';
list1.directory.order = 'lex';

image_list = { list1 };

%%%

dts.initialize();
dts.processImageList(image_list);

%%%

dts_data = dts.getDatasets();
scenario.setDatasets(dts_data);

%%%

scenario.execute();
scenario_data = scenario.getData();

%%%

MkeccScenario.showNodeResults(scenario_description, ...
                              scenario_data, ...
                              dts_data, ...
                              'ccalib');
