function tutorial08

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

ccalib_path = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', 'ccalib');

img_file1 = fullfile(ccalib_path, 'img_0000.png');
img_file2 = fullfile(ccalib_path, 'img_0001.png');

%%%

chess_det.name = 'CHESS_DETECTOR';
chess_det.parameters = struct;

ell_det.name = 'ELLIPSE_DETECTOR';
ell_det.parameters = struct;

dataset1.id = 'dts1';
dataset1.detectors = { chess_det, ell_det };

desc = { dataset1 };

%%%

dts = MkeccDatasets();
dts.setDescription(desc);

dts.initialize();
dts.processImageFile(img_file1, 'dts1');
dts.processImageFile(img_file2, 'dts1');
data = dts.getDatasets();

dts.show(data, 'dts1', 1);

%%% 

w = data.dts1.frames(1).width;
h = data.dts1.frames(1).height;

camera_model.type = 'OPENCV_MODEL';
camera_model.width = w;
camera_model.height = h;
camera_model.k = [w, w, w / 2, h / 2]; 
camera_model.opt = [true, true, true, true, true, true, true, true];
camera_model.dist = [0, 0, 0, 0, 0, 0, 0, 0];

%%%

target = MkeccTarget('ELMARK_CHESSBOARD_TARGET');

target.setCameraModel(camera_model);
target.initialize(data.dts1, 1);

target.detect();

%%%

res = target.getResults();
dts.show(data, 'dts1', 1);
target.show(res);
