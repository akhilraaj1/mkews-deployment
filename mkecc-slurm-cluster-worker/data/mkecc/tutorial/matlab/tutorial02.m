function tutorial02

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

img_file = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', ...
                    'ccalib', 'img_0000.png');
                
%%%                

im = imread(img_file);
imshow(im);
hold('on');

%%%

chess_det = MkeccDetector('CHESS_DETECTOR');
results = chess_det.process(im);
chess_det.show(results);

%%%

ell_det = MkeccDetector('ELLIPSE_DETECTOR');
results = ell_det.process(im);
ell_det.show(results);

