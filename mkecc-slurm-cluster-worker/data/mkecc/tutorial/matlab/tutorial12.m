function tutorial12

node.id = 'ccalib';
node.type = 'CameraCalibration';
node.parameters = struct;
node.required = [];
node.parameters.camera_model_type = 'OPENCV_MODEL';
node.parameters.target_type = 'ELMARK_CHESSBOARD_TARGET';
node.parameters.camera_model_opt = [true, true, true, true, true, true, true, true];
node.parameters.target_parameters.x_mm = 29.63;
node.parameters.target_parameters.y_mm = 29.63;

scenario_description = { node };

session = MkeccSession();
session.setScenarioDescription(scenario_description);

%%%

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

ccalib_path = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', 'ccalib');

list1.dataset = 'ccalib';
list1.directory.path = ccalib_path;
list1.directory.mask = '.*';
list1.directory.order = 'lex';

image_list = { list1 };

%%%

session.initializeDatasets();
session.processImageList(image_list);
session.finalizeDatasets();

%%%

session.execute();

%%%

scenario_data = session.getScenarioData(); 
dts_data = session.getDatasets();

MkeccScenario.showNodeResults(scenario_description, ...
                              scenario_data, ...
                              dts_data, ...
                              'ccalib');
