function tutorial13

%%%

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

template_path = fullfile(path, '..', '..', ...
                         'data', 'templates', 'camera_calib.json');

ccalib_path = fullfile(path, '..', '..', ...
                      'data', 'vcsel_example_01', 'ccalib');

%%%

macros.DATA_PATH = ccalib_path;

session_template = MkeccSessionTemplate();
session_template.setFromFile(template_path);
session = session_template.getInstance(macros);

%%%

ilist = session_template.getImageList(macros);

% Matlab<->JSON workaround
if (numel(ilist) == 1)
    ilist = {ilist};
end

session.initializeDatasets();
session.processImageList(ilist);
session.finalizeDatasets();

%%%

session.execute();

%%%

scenario_description = session.getScenarioDescription(); 
scenario_data = session.getScenarioData(); 
dts_data = session.getDatasets();

MkeccScenario.showNodeResults(scenario_description, ...
                              scenario_data, ...
                              dts_data, ...
                              'ccalib');