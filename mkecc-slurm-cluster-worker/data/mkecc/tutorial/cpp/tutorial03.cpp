#include <string>
#include <iostream>
#include <exception>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "mkecc/util/info.h"
#include "mkecc/dets/factory.h"
using nlohmann::json;

//=============================================================================

int main(int argc, char *argv[])
{
  try
  {
    po::variables_map vm;
    po::options_description desc{"Options"};

    desc.add_options()
        ("help,h",
         "Produce this help message"
         );

    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help"))
      {
        std::cout << "This is MkECC " << argv[0] << ", using "
                  << mke::cc::util::GetInfoString() << std::endl;
        std::cout << desc << std::endl;
        std::exit(0);
      }

    po::notify(vm);

    // Tutorial03

    // Get available detectors
    json avail_dets = mke::cc::dets::Factory::getAvailableDetectors();
    std::cout << "Available detectors:" << std::endl;
    std::cout << avail_dets.dump(2) << std::endl << std::endl;


    // CHESS_DETECTOR
    std::unique_ptr<mke::cc::dets::Detector> chess_det(mke::cc::dets::Factory::create("CHESS_DETECTOR"));

    json meta_info = chess_det->getMetaInfo();
    std::cout << "CHESS_DETECTOR information:" << std::endl;
    std::cout << meta_info.dump(2) << std::endl << std::endl;

    json params = chess_det->getParams();
    std::cout << "CHESS_DETECTOR parameters:" << std::endl;
    std::cout << params.dump(2) << std::endl << std::endl;

    json param_schema = chess_det->getParamSchema();
    std::cout << "CHESS_DETECTOR parameter schema:" << std::endl;
    std::cout << param_schema.dump(2) << std::endl << std::endl;

    // ELLIPSE_DETECTOR
    std::unique_ptr<mke::cc::dets::Detector> ell_det(mke::cc::dets::Factory::create("ELLIPSE_DETECTOR"));

    meta_info = ell_det->getMetaInfo();
    std::cout << "ELLIPSE_DETECTOR information:" << std::endl;
    std::cout << meta_info.dump(2) << std::endl << std::endl;

    params = ell_det->getParams();
    std::cout << "ELLIPSE_DETECTOR parameters:" << std::endl;
    std::cout << params.dump(2) << std::endl << std::endl;

    params["delta"] = 22;
    ell_det->setParams(params);

    param_schema = ell_det->getParamSchema();
    std::cout << "ELLIPSE_DETECTOR parameter schema:" << std::endl;
    std::cout << param_schema.dump(2) << std::endl << std::endl;
  }
  catch (std::exception &ex)
  {
    std::cerr << '\n' << "Error: " << ex.what() << '\n';
    return -1;
  }

  return 0;
}
