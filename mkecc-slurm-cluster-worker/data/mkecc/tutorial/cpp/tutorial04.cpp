#include <string>
#include <iostream>
#include <exception>
#include <vector>

#include <CImg.h>
namespace cimg = cimg_library;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "mkecc/util/info.h"
#include "mkecc/gil/gil.h"
#include "mkecc/dets/datasets.h"
using nlohmann::json;

//=============================================================================

int main(int argc, char *argv[])
{

  try
  {
    po::variables_map vm;
    po::options_description desc{"Options"};

    desc.add_options()
        ("help,h",
         "Produce this help message"
         )
        ("mkecc_root",
         po::value<std::string>(),
         "Path to MkECC installation root"
         );

    po::store(po::parse_command_line(argc, argv, desc), vm);

    if ((argc < 2) || vm.count("help"))
      {
        std::cout << "This is MkECC " << argv[0] << ", using "
                  << mke::cc::util::GetInfoString() << std::endl;
        std::cout << desc << std::endl;
        std::exit(0);
      }

    po::notify(vm);

    fs::path root_path;

    if (vm.count("mkecc_root"))
      {
        root_path = fs::path(vm["mkecc_root"].as<std::string>())
            / "data" / "vcsel_example_01" / "ccalib";
      }
    else
      {
        std::cerr << "Please set '--mkecc_root' to point to MkECC installation root" << std::endl;
        exit(-1);
      }

    // Tutorial04

    json dts_desc = R"JSON(
      [
        {
          "detectors":[
            {
              "name":"CHESS_DETECTOR",
              "parameters":{

              }
            }
          ],
          "id":"dts1"
        }
      ]
    )JSON"_json;

    std::vector<std::string> img_names = {"img_0000.png", "img_0001.png", "img_0002.png"};
    std::vector<fs::path> image_paths;

    for(const auto &img_name : img_names)
      image_paths.push_back(root_path / img_name);

    // Create datasets
    mke::cc::dets::Datasets dts;
    dts.setDescription(dts_desc);
    dts.initialize();

    // Process image files
    for (const auto &img_name : img_names)
      dts.processImageFile((root_path / img_name).string(), "dts1");

    json data = dts.getDatasets();
    json &results = data["/dts1/detectors/0/results"_json_pointer];

    // Show results
    const unsigned char col[] = {255};

    std::vector<cimg::CImg<unsigned char>> images;
    images.resize(img_names.size());

    for (size_t i = 0; i < img_names.size(); i++)
      {
        cimg::CImg<unsigned char> &image = images[i];
        image.load_png(image_paths[i].string().c_str());

        for (const auto &coords : results[i])
          image.draw_circle(int(std::round(coords[0].get<double>())),
              int(std::round(coords[1].get<double>())),
              2, col, 1);
      }

    cimg::CImgDisplay disp0(images[0], image_paths[0].string().c_str()),
        disp1(images[1], image_paths[1].string().c_str()),
        disp2(images[2], image_paths[2].string().c_str());

    while (!disp0.is_closed())
      disp0.wait();
  }
  catch (std::exception &ex)
  {
    std::cerr << '\n' << "Error: " << ex.what() << '\n';
    return -1;
  }

  return 0;
}

