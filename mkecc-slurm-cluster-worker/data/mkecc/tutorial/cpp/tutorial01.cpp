#include <string>
#include <iostream>
#include "mkecc/util/info.h"

// ============================================================================

int main(int argc, char *argv[])
{ 
  std::string info_str = mke::cc::util::GetInfoString();
  std::cout << "MkECC info: " << info_str << std::endl;
  return 0;
}
