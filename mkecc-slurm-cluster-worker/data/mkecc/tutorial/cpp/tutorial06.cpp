#include <string>
#include <iostream>
#include <exception>
#include <vector>

#include <CImg.h>
namespace cimg = cimg_library;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "mkecc/util/info.h"
#include "mkecc/gil/gil.h"
#include "mkecc/dets/datasets.h"
using nlohmann::json;

//=============================================================================

int main(int argc, char *argv[])
{
  try
  {
    po::variables_map vm;
    po::options_description desc{"Options"};

    desc.add_options()
        ("help,h",
         "Produce this help message"
         )
        ("mkecc_root",
         po::value<std::string>(),
         "Path to MkECC installation root"
         );

    po::store(po::parse_command_line(argc, argv, desc), vm);

    if ((argc < 2) || vm.count("help"))
      {
        std::cout << "This is MkECC " << argv[0] << ", using "
                  << mke::cc::util::GetInfoString() << std::endl;
        std::cout << desc << std::endl;
        std::exit(0);
      }

    po::notify(vm);

    fs::path root_path;

    if (vm.count("mkecc_root"))
      {
        root_path = fs::path(vm["mkecc_root"].as<std::string>())
            / "data" / "vcsel_example_01" / "ccalib";
      }
    else
      {
        std::cerr << "Please set '--mkecc_root' to point to MkECC installation root" << std::endl;
        exit(-1);
      }

    // Tutorial06

    json dts_desc = R"JSON(
      [
        {
          "detectors":[
            {
              "name":"CHESS_DETECTOR",
              "parameters":{

              }
            }
          ],
          "id":"dts1"
        }
      ]
    )JSON"_json;

    json image_list = R"JSON(
      [
        {
          "dataset":"dts1",
          "directory":{
            "mask":".*",
            "order":"lex",
            "path":"TBD"
          }
        }
      ]
    )JSON"_json;


    // Create datasets
    mke::cc::dets::Datasets dts;
    dts.setDescription(dts_desc);

    // Process image list
    dts.initialize();

    image_list[0]["directory"]["path"] = fs::canonical(root_path).string();
    dts.processImageList(image_list);

    json data = dts.getDatasets();

    // Show results
    const unsigned char red[] = {255, 0, 0};

    std::vector<int> image_idx = {0, 9};

    std::vector<cimg::CImg<unsigned char>> canvases;
    canvases.resize(image_idx.size());

    std::vector<std::string> image_paths;
    image_paths.resize(image_idx.size());

    json &results = data["/dts1/detectors/0/results"_json_pointer];
    json &frames = data ["/dts1/frames"_json_pointer];

    for (size_t i = 0; i < image_idx.size(); i++)
      {
        auto &uri = frames[image_idx[i]]["uri"];
        image_paths[i] = uri.get<std::string>().substr(7);

        cimg::CImg<unsigned char> image(image_paths[i].c_str());

        canvases[i].assign(image.width(), image.height(), 1, 3);
        canvases[i].draw_image(0, 0, 0, 0, image);
        canvases[i].draw_image(0, 0, 0, 1, image);
        canvases[i].draw_image(0, 0, 0, 2, image);

        for (const auto &coords : results[image_idx[i]])
          canvases[i].draw_circle(int(std::round(coords[0].get<double>())),
              int(std::round(coords[1].get<double>())),
              2, red, 1);
      }

    cimg::CImgDisplay disp0(canvases[0], image_paths[0].c_str()),
        disp1(canvases[1], image_paths[1].c_str());

    while (!disp0.is_closed())
      disp0.wait();
  }
  catch (std::exception &ex)
  {
    std::cerr << '\n' << "Error: " << ex.what() << '\n';
    return -1;
  }

  return 0;
}
