{
  "info" : {
    "author" : "Jan Heller <jan@magik-eye.com>, 2019",
    "description" : "One VCSEL-based laser sensor, OpenCV camera model, using MkE directory structure"
  },
  "macros" : {
    "DATA_PATH" : "DATA_PATH is a mandatory template parameter, please set",
    "SERIAL_NUMBER" : "P000R00S000",
    "MKEDET_PATH" : "${DATA_PATH}",
    "MKEDET_NAME" : "mkedet01_${SERIAL_NUMBER}.bin",
    "CAMERA_TARGET_NAME" : "Einstein",
    "LASER_TARGET_NAME" : "Galileo",
    "CAMERA_MODEL_OPTS": [true, true, true, true, true, true, true, true],
    "LASER_TARGET_GAMMA" : 0.6,
    "LASER_DOTS_MIN_THRESHOLD" : 60,
    "INIT_DOE_CENTER" : [0, 0, 0],
    "VCSEL_TYPE" : "Regular12x10",
    "MIN_DEPTH" : 250,
    "MAX_DEPTH" : 2500,
    "MIN_MASK_DEPTH": 250,
    "MAX_MASK_DEPTH": 2500,
    "PATH_SAMPLING_GAMMA" : 0.6,
    "MIN_JOIN_DISTANCE": 10,
    "AVG_PATH_SAMPLING_STEP" : 0.3,
    "PATH_COLLISION_DIST" : 5,
    "SENSOR_STRIDE" : 752,
    "STORE_DBG_DATA" : true,
    "TARGETS_JSON" : "targets.json",
    "VCSELS_JSON" : "vcsels.json"
  },
  "definitions": {
    "camera_model_type": "OPENCV_MODEL",
    "camera_model_opt": { "$ref": "#/macros/CAMERA_MODEL_OPTS" },
    "camera_target_type": { "$ref" : "${TARGETS_JSON}#/${CAMERA_TARGET_NAME}/type" },
    "camera_target_parameters": {
      "x_mm": { "$ref" : "${TARGETS_JSON}#/${CAMERA_TARGET_NAME}/x_unit" },
      "y_mm": { "$ref" : "${TARGETS_JSON}#/${CAMERA_TARGET_NAME}/y_unit" }
    },
    "laser_target_type": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/type" },
    "laser_target_parameters": {
      "elldet_parameters": {
        "gamma": { "$ref" : "#/macros/LASER_TARGET_GAMMA" }
      },
     "height": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/height" },
      "width": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/width" },
      "x_mm": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/x_unit" },
      "y_mm": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/y_unit" }
    },
    "trgpts_dotdet_parameters": {
       "min_join_distance": { "$ref" : "#/macros/MIN_JOIN_DISTANCE"},
       "min_threshold": { "$ref" : "#/macros/LASER_DOTS_MIN_THRESHOLD" }
    },
    "uncdoe_parameters": {
      "loss_length_scale": 5,
      "min_pts_per_ray": 3,
      "num_iters": 500,
      "num_rays": 3,
      "rays_threshold": 0.0872,
      "init_doe_center" : { "$ref" : "#/macros/INIT_DOE_CENTER" }
    },
    "consdoe_parameters": {
      "doe_pose_type" : "FREE_DOE_POSE",
      "num_stages": 2,
      "free_grid_step_type": { "$ref" : "${VCSELS_JSON}#/${VCSEL_TYPE}/free_grid_step_type" },
      "free_grid_ystep_scale": { "$ref" : "${VCSELS_JSON}#/${VCSEL_TYPE}/free_grid_ystep_scale" }
    },    
    "vcseltopo_parameters": {
      "topology_type" : { "$ref" : "${VCSELS_JSON}#/${VCSEL_TYPE}/topology_type" },
      "num_cols" : { "$ref" : "${VCSELS_JSON}#/${VCSEL_TYPE}/num_cols" },
      "num_rows" : { "$ref" : "${VCSELS_JSON}#/${VCSEL_TYPE}/num_rows" }
    },    
    "ray_sampler_parameters": {
      "path_sampling_gamma" : { "$ref" : "#/macros/PATH_SAMPLING_GAMMA" },
      "avg_path_sampling_step": { "$ref" : "#/macros/AVG_PATH_SAMPLING_STEP" },
      "far_shoulder_length": 1,
      "max_depth": { "$ref" : "#/macros/MAX_DEPTH" },
      "min_depth": { "$ref" : "#/macros/MIN_DEPTH" },
      "max_mask_depth": { "$ref" : "#/macros/MAX_MASK_DEPTH" },
      "min_mask_depth": { "$ref" : "#/macros/MIN_MASK_DEPTH" },
      "near_shoulder_length": 5,
      "path_collision_dist":  { "$ref" : "#/macros/PATH_COLLISION_DIST" },
      "build_pxlists": false
    },
    "mkedet01_parameters": {
      "dd_section_type": "DD_SECTION_COLLUT",
      "file_name": "${MKEDET_NAME}",
      "file_path": "${MKEDET_PATH}",
      "info_data": {
        "hostname": "${HOSTNAME_}",
        "serial_number": "${SERIAL_NUMBER}",
        "timestamp": "${TIMESTAMP_}",
        "system": "${SYSTEM_}",
        "username": "${USERNAME_}",
        "mkecc_version" : "${MKECC_VERSION_}"
      },
      "laser_pattern": [ [ 1 ] ],
      "pp_section_type": "PP_SECTION_EMPTY",
      "stride": { "$ref" : "#/macros/SENSOR_STRIDE" }
    }
  },
  "scenario" : {
    "parameters" : {
      "store_dbg_data" : { "$ref" : "#/macros/STORE_DBG_DATA" }
    },
    "description" : [
    {
      "id": "ccalib",
      "parameters": {
        "camera_model_opt": { "$ref": "#/macros/CAMERA_MODEL_OPTS" },
        "camera_model_type": { "$ref": "#/definitions/camera_model_type" },
        "target_parameters": { "$ref": "#/definitions/camera_target_parameters" },
        "target_type": { "$ref": "#/definitions/camera_target_type" }
      },
      "required": [],
      "type": "CameraCalibration"
    },
    {
      "id": "pattern1_trgpts",
      "parameters": {
        "dotdet_parameters": { "$ref": "#/definitions/trgpts_dotdet_parameters" },
        "target_parameters": { "$ref": "#/definitions/laser_target_parameters" },
        "target_type": { "$ref": "#/definitions/laser_target_type" }
      },
      "required": [ "ccalib" ],
      "type": "TargetPoints"
    },
    {
      "id": "pattern1_uncdoe",
      "parameters": { "$ref": "#/definitions/uncdoe_parameters" },
      "required": [ "pattern1_trgpts" ],
      "type": "UnconstrainedDoeCalib"
    },
    {
      "id": "pattern1_vcseltopo",
      "parameters": { "$ref": "#/definitions/vcseltopo_parameters" },
      "required": [ "pattern1_uncdoe" ],
      "type": "VcselTopology"
    },
    {
      "id": "pattern1_consdoe",
      "parameters": { "$ref": "#/definitions/consdoe_parameters" },
      "required": [ "pattern1_vcseltopo" ],
      "type": "ConstrainedDoeCalib"
    },    
    {
      "id": "merge_doe",
      "parameters": {
        "ray_groups": [
          [
            {
              "doe_idx": 0,
              "group_idx": 0,
              "node_id": "pattern1_consdoe"
            }
          ]
        ]
      },
      "required": [ "pattern1_consdoe" ],
      "type": "DoeGroupsMerge"
    },
    {
      "id": "ray_sampler",
      "parameters": { "$ref": "#/definitions/ray_sampler_parameters" },
      "required": [ "merge_doe" ],
      "type": "RaySampler"
    },
    {
      "id": "mkedet01",
      "parameters": { "$ref": "#/definitions/mkedet01_parameters" },
      "required": [ "ray_sampler" ],
      "type": "MkedetExporter"
    }
  ]
  },
  "image_list" : [
    {
      "dataset" : "ccalib",
      "directory" : {
        "path" : "${DATA_PATH}/ccalib",
        "mask" : ".*",
        "order" : "lex"
      }
    },
    {
      "dataset" : "pattern1_trgpts_targets",
      "directory" : {
        "path" : "${DATA_PATH}/pattern1/target",
        "mask" : ".*",
        "order" : "lex"
      }
    },
    {
      "dataset" : "pattern1_trgpts_dots",
      "directory" : {
        "path" : "${DATA_PATH}/pattern1/laser",
        "mask" : ".*",
        "order" : "lex"
      }
    }        
  ]
}
