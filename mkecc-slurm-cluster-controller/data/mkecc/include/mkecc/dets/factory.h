/* 
 * DetectorFactory: factory for instantiating detector objects
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */


#ifndef _DETS_FACTORY_H_
#define _DETS_FACTORY_H_

#include <map>
#include "mkecc/dets/detector.h"
#include "mkecc/json.hpp"

namespace mke {
namespace cc {
namespace dets {
    
/** \addtogroup mkecc
 *  @{
 */
    
class Factory {
public:    
  static Detector* create(const std::string &name);
  static Detector* create(const DetectorType type);
    
  static nlohmann::json getAvailableDetectors(void); 
  
private:
  const static std::map<const std::string, Detector*(*)(void)> create_map_;
  const static std::map<DetectorType, Detector*(*)(void)> type_map_;
};    


/** @}*/


} // namespace dets
} // namespace cc
} // namespace mke

#endif // _DETS_FACTORY_H_
