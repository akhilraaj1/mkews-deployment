/* 
 * Detector: Base class for all detectors
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _MKECC_DETECTOR_H_
#define _MKECC_DETECTOR_H_

#include "mkecc/json.hpp"
#include "mkecc/gil/gil.h"
#include "mkecc/util/exception.h"

namespace mke {
namespace cc {
namespace dets {

/** \addtogroup mkecc
 *  @{
 */

typedef enum {
  UNKNOWN_DETECTOR = 0,
  DOT_DETECTOR = 1,
  ELLIPSE_DETECTOR = 2,
  CHESS_DETECTOR = 3,
  ARUCO_DETECTOR = 4,
  OCVCHESSBOARD_DETECTOR = 5,
  MSER_DETECTOR = 6
} DetectorType;    
      
class Detector {
public:

  // Construction ============================================================  
  
  Detector();
  virtual ~Detector();
  
  // Assignment operators =====================================================
  
  Detector& operator=(const Detector& detector) = delete;
  Detector& operator=(Detector &&detector) noexcept;  
  
  // Base methods =============================================================
  
  nlohmann::json getParams() const;
  void setParams(const nlohmann::json &params);
  void validateParams(const nlohmann::json &params);
  nlohmann::json getParamSchema() const;
  nlohmann::json getMetaInfo() const;
  
  // Virtual methods ==========================================================
  
  virtual const std::string& getName(void) const = 0;
  virtual DetectorType getType(void) const = 0;
  virtual void process(const mke::cc::gil::ImageView& iview, nlohmann::json &results) = 0;
  
  // Implementation ===========================================================
  
protected:
  struct DetectorImpl;  // Forward declaration of the implementation class
  DetectorImpl *impl_; // PIMPL  
};

/** @}*/

} // namespace dets
} // namespace cc
} // namespace mke

#endif
