/* 
 * CameraBase: A base class for a camera
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _CAMERABASE_H_
#define _CAMERABASE_H_

#include "mkecc/json.hpp"
#include "mkecc/camera/model.h"

namespace mke {
namespace cc {
namespace camera {
    
/** \addtogroup mkecc
 *  @{
 */

typedef enum { 
  FPROJECTION_NONNORMALIZED = 0,
  FPROJECTION_NORMALIZED_TO_ONE = 1,
  FPROJECTION_NORMALIZED_TO_Z = 2
} ForwardProjectionType; 

      
class CameraBase {
public:

  // Construction ============================================================  
  
  CameraBase();  
  CameraBase(const CameraBase &camerabase);
  virtual ~CameraBase();
  
  CameraBase& operator=(const CameraBase& camera) = delete;
  CameraBase& operator=(CameraBase &&camera) noexcept = delete;
  
  virtual void get(nlohmann::json &camera) const = 0;
  virtual void set(const nlohmann::json &camera) = 0;
  virtual void validate(const nlohmann::json &camera) const = 0;
  virtual const nlohmann::json& getSchema(void) const = 0;
 
  void setModel(std::shared_ptr<mke::cc::camera::Model> model_ptr);
  void setModel(const mke::cc::camera::Model &model);
  std::shared_ptr<mke::cc::camera::Model> getModel(void) const;
  
  bool isValid(void) const;
  void setValidityFlag(bool vflag);

  void forwardProject(const double* const u2d, double* const v3d, const int no_pts, ForwardProjectionType fptype = FPROJECTION_NONNORMALIZED) const;  
  
protected:
  struct ImplBase;      // Forward declaration of the implementation class
  ImplBase *implbase_;  // PIMPL  
};

/** @}*/

} // namespace camera
} // namespace cc
} // namespace mke

#endif // _CAMERABASE_H_
