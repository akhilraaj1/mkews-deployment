/* 
 * Camera: A class to represent a camera, i.e, camera model (intrinsics) + camera pose (extrinsics)
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */


#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "mkecc/camera/camerabase.h"
#include "mkecc/geom/etrans3d.h"

namespace mke {
namespace cc {
namespace camera {
    
/** \addtogroup mkecc
 *  @{
 */
      
class Camera : public CameraBase {
public:

  // Construction ============================================================  
  
  Camera();  
  Camera(const Camera &camera);
  virtual ~Camera();
  
  Camera& operator=(const Camera& camera);
  Camera& operator=(Camera &&camera) noexcept;
  
  void get(nlohmann::json &out_camera) const;
  void set(const nlohmann::json &camera);
  
  void validate(const nlohmann::json &camera) const;
  const nlohmann::json& getSchema(void) const;
  static void validateStatic(const nlohmann::json &model);
  static const nlohmann::json& getSchemaStatic(void);
  
  void setPose(const mke::cc::geom::EuclideanTransform3D &pose);
  mke::cc::geom::EuclideanTransform3D* getPose(void) const;
  
  void project(const double* const u3d, double* const v2d, const int no_pts) const;
    
private:
  struct Impl;  // Forward declaration of the implementation class
  Impl *impl_;  // PIMPL      
};

/** @}*/

} // namespace camera
} // namespace cc
} // namespace mke

#endif // _CAMERA_H_
