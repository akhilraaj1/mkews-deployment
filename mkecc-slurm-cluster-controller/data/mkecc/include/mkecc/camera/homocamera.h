/* 
 * HomographyCamera: A class to represent a homograpy-based camera, i.e, camera model (intrinsics) + homography (extrinsics)
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _HOMOCAMERA_H_
#define _HOMOCAMERA_H_

#include "mkecc/camera/camerabase.h"
#include "mkecc/geom/homography.h"

namespace mke {
namespace cc {
namespace camera {
    
/** \addtogroup mkecc
 *  @{
 */
      
class HomographyCamera : public CameraBase {
public:

  // Construction ============================================================  
  
  HomographyCamera();  
  HomographyCamera(const HomographyCamera &camera);
  virtual ~HomographyCamera();
  
  HomographyCamera& operator=(const HomographyCamera& camera);
  HomographyCamera& operator=(HomographyCamera &&camera) noexcept;
  
  void get(nlohmann::json &out_camera) const;
  void set(const nlohmann::json &camera);
  
  void validate(const nlohmann::json &camera) const;
  const nlohmann::json& getSchema(void) const;
  static void validateStatic(const nlohmann::json &model);
  static const nlohmann::json& getSchemaStatic(void);
  
  void setHomography(const mke::cc::geom::Homography &homography);
  mke::cc::geom::Homography* getHomography(void) const;

  void recover(const double* const u, double* const v, const int no_pts);
  
  void project(const double* const u, double* const v, const int no_pts) const;
  void forwardProjectAndTransform(const double* const u, double* const v, const int no_pts) const;
    
private:
  struct Impl;  // Forward declaration of the implementation class
  Impl *impl_;  // PIMPL      
};

/** @}*/

} // namespace camera
} // namespace cc
} // namespace mke

#endif // _HOMOCAMERA_H_
