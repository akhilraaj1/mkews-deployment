/*
 * Session
 *
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _SESSION_H_
#define _SESSION_H_

#include <memory>
#include <string>

#include "mkecc/json.hpp"
#include "mkecc/gil/gil.h"


namespace mke {
namespace cc {
namespace session {

/** \addtogroup mkecc
 *  @{
 */

class Session {
public:

  // Construction =============================================================

  Session();
  Session(const Session&) = delete;
  Session(Session&&) = default;
  ~Session();

  Session& operator=(const Session&) = delete;
  Session& operator=(Session &&) = default;

  // MKEDS ====================================================================

  std::string addKeyPair(const nlohmann::json &key_pair);
  std::string addDefaultEncryptedKeyPair(const nlohmann::json &key_pair);
  std::string addEncryptedKeyPair(const nlohmann::json &key_pair_json, const std::string &password);
  void saveEncrypted(const std::string &fname, const std::string &hash);

  // Storage ==================================================================

  void load(const std::string &fname);
  void loadData(const std::string &fname);
  void save(const std::string &fname);

  // Data =====================================================================

  void setData(const std::string &json_pointer_str, const nlohmann::json &data);
  const nlohmann::json& getData(const std::string &json_pointer_str) const;
  const nlohmann::json& getInfoData(void) const;
  void swapData(const std::string json_pointer_str, nlohmann::json &swap_data);

  // Datasets =================================================================

  const nlohmann::json& getDatasetsDescription(void) const;
  void initializeDatasets(void);
  const std::string& getFrameRetentionRoot(void) const;
  void setFrameRetentionRoot(const std::string &froot);
  void processFrame(const mke::cc::gil::ImageView& iview, const std::string &dtsid);
  void processImageFile(const std::string &filepath, const std::string &dtsid);
  void processImageList(const nlohmann::json &ilist);

  void finalizeDatasets(void);
  const nlohmann::json& getDatasets(void);

  // Scenario =================================================================

  void setScenarioDescription(const nlohmann::json &desc);
  const nlohmann::json& getScenarioDescription(void) const;
  const nlohmann::json& getScenarioData(void) const;
  nlohmann::json findScenarioData(const std::string &key) const;

  void setScenarioParams(const nlohmann::json &params);
  nlohmann::json getScenarioParams(void) const;
  nlohmann::json getScenarioParamsSchema(void) const;

  void setNodeParams(const std::string &node_id, const nlohmann::json &params);
  nlohmann::json getNodeParams(const std::string &node_id) const;
  nlohmann::json getNodeParamsSchema(const std::string &node_id) const;
  bool hasNode(const std::string &node_id) const;

  void execute(void);
  void execute(const nlohmann::json &ids);
  void executeIfEmpty(const std::string &id);

  // Session manipulation =====================================================

  Session* getStrippedSession(void);
  Session* getDatasetsSession(void);

  // operators ================================================================

  friend std::ostream& operator<<(std::ostream& o, Session &session);
  friend std::istream& operator>>(std::istream& i, Session &session);

private:
  struct Impl;  // Forward declaration of the implementation class
//  std::unique_ptr<Impl * impl_; // PIMPL
  Impl * impl_; // PIMPL
};

std::ostream& operator<<(std::ostream& o, Session &session);
std::istream& operator>>(std::istream& i, Session &session);

/** @}*/

} // namespace session
} // namespace cc
} // namespace mke

#endif // _SESSION_H_
