/* 
 * InvertibleTransformation: A class interface for an invertible transfrom concept
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */


#ifndef _ITRANS_H_
#define _ITRANS_H_

#include "mkecc/json.hpp"
#include "mkecc/geom/trans.h"

namespace mke {
namespace cc {
namespace geom {

/** \addtogroup mkecc
 *  @{
 */

class InvertibleTransformation : public Transformation {
public:
  virtual ~InvertibleTransformation()
    {}

  InvertibleTransformation& operator=(const InvertibleTransformation& trans) = delete;
  InvertibleTransformation& operator=(InvertibleTransformation &&trans) noexcept = delete;      
    
  virtual void invert(void) = 0;  
  virtual void inverseTransformPoints(const double* const u, double* const v, const size_t no_pts) const = 0;
};


/** @}*/

} // namespace geom
} // namespace cc
} // namespace mke

#endif // _ITRANS_H_
