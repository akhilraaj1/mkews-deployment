/* 
 * Transformation: A base class interface for a transfrom concept
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */


#ifndef _TRANS_H_
#define _TRANS_H_

#include "mkecc/json.hpp"

namespace mke {
namespace cc {
namespace geom {

/** \addtogroup mkecc
 *  @{
 */

class Transformation {
public:
  virtual ~Transformation()
    {}
    
  Transformation& operator=(const Transformation& trans) = delete;
  Transformation& operator=(Transformation &&trans) noexcept = delete;  
  
  virtual void get(nlohmann::json &trans) const = 0;
  virtual void set(const nlohmann::json &trans) = 0;
  virtual void validateDynamic(const nlohmann::json &trans) const = 0;
  virtual const nlohmann::json& getSchemaDynamic(void) const = 0;
  
  virtual void transformPoints(const double* const u, double* const v, const size_t no_pts) const = 0;
};


/** @}*/

} // namespace geom
} // namespace cc
} // namespace mke

#endif // _TRANS_H_
