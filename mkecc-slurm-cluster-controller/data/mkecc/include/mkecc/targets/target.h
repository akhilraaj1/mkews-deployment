/* 
 * Target: Base class for all detectors
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _TARGET_H_
#define _TARGET_H_

#include "mkecc/util/exception.h"
#include "mkecc/json.hpp"
#include "mkecc/camera/model.h"

namespace mke {
namespace cc {
namespace targets {

/** \addtogroup mkecc
 *  @{
 */

typedef enum { 
  UNKNOWN_TARGET = 0,
  ELMARK_CHESSBOARD_TARGET = 1,
  ELMARK_CIRCLE_TARGET = 2,
  ELMARK_CORNERS_TARGET = 3,
  OPENCV_CHESSBOARD_TARGET = 4,
  CHESSBOARD_TARGET = 5,
  CIRCCODE_CORNERS_TARGET = 6,
} TargetType;    
      
class Target {
public:

  // Construction ============================================================  
  
  Target();
  Target(const Target &target) = delete;
  Target(Target &&target) = default;
  virtual ~Target();
  
  // Assignment operators =====================================================
  
  Target& operator=(const Target& target) = delete;
  Target& operator=(Target &&target) = default;
  
  // Base methods =============================================================
  
  nlohmann::json getParams() const;
  virtual void setParams(const nlohmann::json &params);
  void validateParams(const nlohmann::json &params);
  nlohmann::json getParamSchema() const;
  nlohmann::json getMetaInfo() const;
  
  void setCameraModel(const mke::cc::camera::Model &model);
  mke::cc::camera::Model getCameraModel(void) const;
  
  // Virtual methods ==========================================================
  
  virtual const std::string& getName(void) const = 0;
  virtual TargetType getType(void) const = 0;

  /**
  * @brief Returns true if the target is planar, i.e., all target points have
  * zero z-coordinate. Also, z-axis must point "towards" the camera, i.e.,
  * if a camera position is recovered observing the target, the camera pose
  * will have positive z-coordinate in the target coordinate system.
  */
  virtual bool isPlanar(void) const = 0;
  
  /**
  * @brief Returns @nlohmann::json data structure providing detailed data describing the 
  * current state of the target recovery (Marker indices marker connections, etc.).
  * 
  * @param data data: @nlohmann::json data structure describing internal state 
  */
  virtual void getStateData(nlohmann::json &out_data) = 0;

  virtual bool initialize(const nlohmann::json &dts, const size_t frame_idx) = 0;
  virtual bool detect(void) = 0;
  virtual void getResults(nlohmann::json &results) = 0;

  virtual bool intersectRay(const double* const orig,
                            const double* const dir,
                            double* const ipt) = 0;
  
  // Implementation ===========================================================
  
protected:
  struct Impl;  // Forward declaration of the implementation class
  std::unique_ptr<Impl> impl_; // PIMPL
};

/** @}*/

} // namespace targets
} // namespace cc
} // namespace mke

#endif
