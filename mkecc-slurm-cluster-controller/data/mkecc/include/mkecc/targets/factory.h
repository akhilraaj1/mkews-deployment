/* 
 * TargetFactory: factory for instantiating target objects
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */


#ifndef _TARGET_FACTORY_H
#define _TARGET_FACTORY_H

#include <map>
#include "mkecc/targets/target.h"
#include "mkecc/json.hpp"

namespace mke {
namespace cc {
namespace targets {
    
/** \addtogroup mkecc
 *  @{
 */
    
class Factory {
public:    
  static Target* create(const std::string &name);
  static Target* create(const TargetType type);
    
  static nlohmann::json getAvailableTargets(void); 
  
private:
  const static std::map<const std::string, Target*(*)(void)> create_map_;
  const static std::map<TargetType, Target*(*)(void)> type_map_;
};    


/** @}*/


} // namespace targets
} // namespace cc
} // namespace mke

#endif // _TARGET_FACTORY_H
