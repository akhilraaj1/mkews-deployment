{
  "info" : {
    "author" : "Jan Heller <jan@magik-eye.com>, 2019",
    "description" : "One VCSEL-based laser sensor, OpenCV camera model, using MkE directory structure"
  },
  "macros" : {
    "DATA_PATH" : "DATA_PATH is a mandatory template parameter, please set",
    "SERIAL_NUMBER" : "P000R00S000",
    "MKEDET_PATH" : "${DATA_PATH}",
    "MKEDET_NAME" : "mkedet01_${SERIAL_NUMBER}.bin",
    "CAMERA_TARGET_NAME" : "EinsteinX",
    "LASER_TARGET_NAME" : "Galileo",
    "CAMERA_MODEL_OPTS": [true, true, true, true, true, true, true, true],
    "LASER_TARGET_GAMMA" : 0.6,
    "LASER_DOTS_MIN_THRESHOLD" : 60,
    "INIT_DOE_CENTER" : [0, 0, 0],
    "MAX_ANGULAR_DISTANCE" :  0.0087,
    "RAYS_THRESHOLD" : 0.0872,
    "MIN_PTS_PER_RAY" : 3,
    "RAY_INTERPOLATION_LEVEL" : 2,
    "MIN_DEPTH" : 250,
    "MAX_DEPTH" : 2500,
    "MIN_MASK_DEPTH": 250,
    "MAX_MASK_DEPTH": 2500,
    "PATH_SAMPLING_GAMMA" : 1,
    "MIN_JOIN_DISTANCE": 10,
    "AVG_PATH_SAMPLING_STEP" : 0.2,
    "PATH_COLLISION_DIST" : 5,
    "FAR_SHOULDER_LENGTH" : 5,
    "NEAR_SHOULDER_LENGTH" : 5,
    "SENSOR_STRIDE" : 752,
    "DATA3D_TYPE" : "DD_DATA3D_MM16",
    "STORE_DBG_DATA" : true,
    "TARGETS_JSON" : "targets.json"
  },
  "definitions": {
    "camera_model_type": "OPENCV_MODEL",
    "camera_model_opt": { "$ref": "#/macros/CAMERA_MODEL_OPTS" },
    "camera_target_type": { "$ref" : "${TARGETS_JSON}#/${CAMERA_TARGET_NAME}/type" },
    "camera_target_parameters": {
      "x_mm": { "$ref" : "${TARGETS_JSON}#/${CAMERA_TARGET_NAME}/x_unit" },
      "y_mm": { "$ref" : "${TARGETS_JSON}#/${CAMERA_TARGET_NAME}/y_unit" }
    },
    "laser_target_type": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/type" },
    "laser_target_parameters": {
      "elldet_parameters": {
        "gamma": { "$ref" : "#/macros/LASER_TARGET_GAMMA" }
      },
     "height": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/height" },
      "width": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/width" },
      "x_mm": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/x_unit" },
      "y_mm": { "$ref" : "${TARGETS_JSON}#/${LASER_TARGET_NAME}/y_unit" }
    },
    "trgpts_dotdet_parameters": {
       "min_join_distance": { "$ref" : "#/macros/MIN_JOIN_DISTANCE"},
       "min_threshold": { "$ref" : "#/macros/LASER_DOTS_MIN_THRESHOLD" }
    },
    "uncdoe_parameters": {
      "loss_length_scale": 5,
      "min_pts_per_ray": { "$ref" : "#/macros/MIN_PTS_PER_RAY" },
      "num_iters": 500,
      "num_rays": 5,
      "rays_threshold": { "$ref" : "#/macros/RAYS_THRESHOLD" },
      "max_angular_distance" : { "$ref" : "#/macros/MAX_ANGULAR_DISTANCE" },
      "init_doe_center" : { "$ref" : "#/macros/INIT_DOE_CENTER" }
    },
    "raytopo_parameters": {
      "ray_interpolation_level" : { "$ref" : "#/macros/RAY_INTERPOLATION_LEVEL" },
      "reserved_ids_per_ray" : 0
    },
    "ray_sampler_parameters": {
      "path_sampling_gamma" : { "$ref" : "#/macros/PATH_SAMPLING_GAMMA" },
      "avg_path_sampling_step": { "$ref" : "#/macros/AVG_PATH_SAMPLING_STEP" },
      "far_shoulder_length": { "$ref" : "#/macros/FAR_SHOULDER_LENGTH" },
      "max_depth": { "$ref" : "#/macros/MAX_DEPTH" },
      "min_depth": { "$ref" : "#/macros/MIN_DEPTH" },
      "max_mask_depth": { "$ref" : "#/macros/MAX_MASK_DEPTH" },
      "min_mask_depth": { "$ref" : "#/macros/MIN_MASK_DEPTH" },
      "near_shoulder_length": { "$ref" : "#/macros/NEAR_SHOULDER_LENGTH" },
      "path_collision_dist":  { "$ref" : "#/macros/PATH_COLLISION_DIST" },
      "build_pxlists": false
    },
    "mkedet01_parameters": {
      "dd_section_type": "DD_SECTION_INTLUT",
      "file_name": "${MKEDET_NAME}",
      "file_path": "${MKEDET_PATH}",
      "data3d_type" : { "$ref" : "#/macros/DATA3D_TYPE" },
      "info_data": {
        "hostname": "${HOSTNAME_}",
        "serial_number": "${SERIAL_NUMBER}",
        "timestamp": "${TIMESTAMP_}",
        "system": "${SYSTEM_}",
        "username": "${USERNAME_}",
        "mkecc_version" : "${MKECC_VERSION_}"
      },
      "laser_pattern": [ [ 1 ] ],
      "pp_section_type": "PP_SECTION_EMPTY",
      "stride": { "$ref" : "#/macros/SENSOR_STRIDE" }
    }
  },
  "scenario" : {
    "parameters" : {
      "store_dbg_data" : { "$ref" : "#/macros/STORE_DBG_DATA" }
    },
    "description" : [
    {
      "id": "ccalib",
      "parameters": {
        "camera_model_opt": { "$ref": "#/macros/CAMERA_MODEL_OPTS" },
        "camera_model_type": { "$ref": "#/definitions/camera_model_type" },
        "target_parameters": { "$ref": "#/definitions/camera_target_parameters" },
        "target_type": { "$ref": "#/definitions/camera_target_type" }
      },
      "required": [],
      "type": "CameraCalibration"
    },
    {
      "id": "pattern1_trgpts",
      "parameters": {
        "dotdet_parameters": { "$ref": "#/definitions/trgpts_dotdet_parameters" },
        "target_parameters": { "$ref": "#/definitions/laser_target_parameters" },
        "target_type": { "$ref": "#/definitions/laser_target_type" }
      },
      "required": [ "ccalib" ],
      "type": "TargetPoints"
    },
    {
      "id": "pattern1_uncdoe",
      "parameters": { "$ref": "#/definitions/uncdoe_parameters" },
      "required": [ "pattern1_trgpts" ],
      "type": "UnconstrainedDoeCalib"
    },
    {
      "id": "pattern1_raytopo",
      "parameters": { "$ref": "#/definitions/raytopo_parameters" },
      "required": [ "pattern1_uncdoe" ],
      "type": "RayTopology"
    },    
    {
      "id": "merge_doe",
      "parameters": {
        "ray_groups": [
          [
            {
              "doe_idx": 0,
              "group_idx": 0,
              "node_id": "pattern1_raytopo"
            }
          ]
        ]
      },
      "required": [ "pattern1_raytopo" ],
      "type": "DoeGroupsMerge"
    },
    {
      "id": "ray_sampler",
      "parameters": { "$ref": "#/definitions/ray_sampler_parameters" },
      "required": [ "merge_doe" ],
      "type": "RaySampler"
    },
    {
      "id": "mkedet01",
      "parameters": { "$ref": "#/definitions/mkedet01_parameters" },
      "required": [ "ray_sampler" ],
      "type": "MkedetExporter"
    }
  ]
  },
  "image_list" : [
    {
      "dataset" : "ccalib",
      "directory" : {
        "path" : "${DATA_PATH}/ccalib",
        "mask" : ".*",
        "order" : "lex"
      }
    },
    {
      "dataset" : "pattern1_trgpts_targets",
      "directory" : {
        "path" : "${DATA_PATH}/pattern1/target",
        "mask" : ".*",
        "order" : "lex"
      }
    },
    {
      "dataset" : "pattern1_trgpts_dots",
      "directory" : {
        "path" : "${DATA_PATH}/pattern1/laser",
        "mask" : ".*",
        "order" : "lex"
      }
    }        
  ]
}
