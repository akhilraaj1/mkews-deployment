#include <string>
#include <iostream>
#include <exception>

#include <CImg.h>
namespace cimg = cimg_library;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "mkecc/util/info.h"
#include "mkecc/gil/gil.h"
#include "mkecc/dets/factory.h"
using nlohmann::json;

//=============================================================================

int main(int argc, char *argv[])
{
  try
  {
    po::variables_map vm;
    po::options_description desc{"Options"};

    desc.add_options()
        ("help,h",
         "Produce this help message"
         )
        ("mkecc_root",
         po::value<std::string>(),
         "Path to MkECC installation root"
         );

    po::store(po::parse_command_line(argc, argv, desc), vm);

    if ((argc < 2) || vm.count("help"))
      {
        std::cout << "This is MkECC " << argv[0] << ", using "
                  << mke::cc::util::GetInfoString() << std::endl;
        std::cout << desc << std::endl;
        std::exit(0);
      }

    po::notify(vm);

    fs::path root_path;

    if (vm.count("mkecc_root"))
      {
        root_path = fs::path(vm["mkecc_root"].as<std::string>());
      }
    else
      {
        std::cerr << "Please set '--mkecc_root' to point to MkECC installation root" << std::endl;
        exit(-1);
      }

    // Tutorial02

    fs::path img_path = root_path / "data" / "vcsel_example_01" / "ccalib" / "img_0000.png";

    // Load image data
    cimg::CImg<unsigned char> image(img_path.string().c_str());

    // Prepare image data GIL view
    mke::cc::gil::ImageView view = mke::cc::gil::GetImageView(image);

    // Create CHESS_DETECTOR
    std::unique_ptr<mke::cc::dets::Detector> chess_det(mke::cc::dets::Factory::create("CHESS_DETECTOR"));

    // Run detector on the image
    json chess_results;
    chess_det->process(view, chess_results);

    // Create ELLIPSE_DETECTOR
    std::unique_ptr<mke::cc::dets::Detector> ell_det(mke::cc::dets::Factory::create("ELLIPSE_DETECTOR"));

    // Run detector on the image
    json ell_results;
    ell_det->process(view, ell_results);

    // Show results
    const unsigned char red[] = {255, 0, 0};
    const unsigned char blue[] = {0, 0, 255};

    cimg::CImg<unsigned char> canvas(image.width(), image.height(), 1, 3);
    canvas.draw_image(0, 0, 0, 0, image);
    canvas.draw_image(0, 0, 0, 1, image);
    canvas.draw_image(0, 0, 0, 2, image);

    for (const auto &coords : chess_results)
      canvas.draw_circle(int(std::round(coords[0].get<double>())),
          int(std::round(coords[1].get<double>())),
          1,
          red, 1);

    for (const auto &coords : ell_results)
      canvas.draw_circle(int(std::round(coords[0].get<double>())),
          int(std::round(coords[1].get<double>())),
          1,
          blue, 1);

    cimg::CImgDisplay main_disp(canvas, img_path.string().c_str());

    while (!main_disp.is_closed())
      main_disp.wait();
  }
  catch (std::exception &ex)
  {
    std::cerr << '\n' << "Error: " << ex.what() << '\n';
    return -1;
  }

  return 0;
}
