#include <string>
#include <iostream>
#include <exception>
#include <vector>

#include <CImg.h>
namespace cimg = cimg_library;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "mkecc/util/info.h"
#include "mkecc/gil/gil.h"
#include "mkecc/dets/datasets.h"
#include "mkecc/targets/factory.h"

using nlohmann::json;

//=============================================================================

int main(int argc, char *argv[])
{
  try
  {
    po::variables_map vm;
    po::options_description desc{"Options"};

    desc.add_options()
        ("help,h",
         "Produce this help message"
         )
        ("mkecc_root",
         po::value<std::string>(),
         "Path to MkECC installation root"
         );

    po::store(po::parse_command_line(argc, argv, desc), vm);

    if ((argc < 2) || vm.count("help"))
      {
        std::cout << "This is MkECC " << argv[0] << ", using "
                  << mke::cc::util::GetInfoString() << std::endl;
        std::cout << desc << std::endl;
        std::exit(0);
      }

    po::notify(vm);

    fs::path root_path;

    if (vm.count("mkecc_root"))
      {
        root_path = fs::path(vm["mkecc_root"].as<std::string>())
            / "data" / "vcsel_example_01" / "ccalib";
      }
    else
      {
        std::cerr << "Please set '--mkecc_root' to point to MkECC installation root" << std::endl;
        exit(-1);
      }

    // Tutorial08

    json dts_desc = R"JSON(
      [
        {
          "detectors":[
            {
              "name":"CHESS_DETECTOR",
              "parameters":{

              }
            },
            {
              "name":"ELLIPSE_DETECTOR",
              "parameters":{

              }
            }
          ],
          "id":"dts1"
        }
      ]
    )JSON"_json;

    std::vector<std::string> img_names = {"img_0000.png", "img_0001.png"};
    std::vector<fs::path> image_paths;

    for(const auto &img_name : img_names)
      image_paths.push_back(root_path / img_name);

    // Create datasets
    mke::cc::dets::Datasets dts;
    dts.setDescription(dts_desc);

    // Process images
    dts.initialize();

    for (const auto &img_name : img_names)
      dts.processImageFile((root_path / img_name).string(), "dts1");

    const json &data = dts.getDatasets();

    // Setup target parameters
    // Note that ELMARK_CHESSBOARD_TARGET target requires camera model

    const json &frame = data["/dts1/frames/0"_json_pointer];
    double width = frame.at("width").get<double>();
    double height = frame.at("height").get<double>();
    int image_idx = 0;

    json camera_model_json;
    camera_model_json["type"] = "OPENCV_MODEL";
    camera_model_json["width"] = width;
    camera_model_json["height"] = height;
    camera_model_json["k"] = {width, width, width / 2.0, width / 2.0};
    camera_model_json["opt"] = {true, true, true, true, true, true, true, true};
    camera_model_json["dist"] = {0, 0, 0, 0, 0, 0, 0, 0};

    mke::cc::camera::Model camera_model;
    camera_model.set(camera_model_json);

    // Detect target
    std::unique_ptr<mke::cc::targets::Target> target(mke::cc::targets::Factory::create("ELMARK_CHESSBOARD_TARGET"));
    target->setCameraModel(camera_model);
    target->initialize(data.at("dts1"), image_idx);
    target->detect();

    json results;
    target->getResults(results);

    // Show results
    const unsigned char red[] = {255, 0, 0};
    auto &frames = data["/dts1/frames"_json_pointer];
    auto &uri = frames[image_idx]["uri"];
    std::string image_path = uri.get<std::string>().substr(7);
    cimg::CImg<unsigned char> image(image_path.c_str());

    cimg::CImg<unsigned char> canvas;

    canvas.assign(image.width(), image.height(), 1, 3);
    canvas.draw_image(0, 0, 0, 0, image);
    canvas.draw_image(0, 0, 0, 1, image);
    canvas.draw_image(0, 0, 0, 2, image);

    for (const auto &coords : results["image_coords"])
      canvas.draw_circle(int(std::round(coords[0].get<double>())),
          int(std::round(coords[1].get<double>())),
          2, red, 1);

    cimg::CImgDisplay disp(canvas, image_path.c_str());

    while (!disp.is_closed())
      disp.wait();
  }
  catch (std::exception &ex)
  {
    std::cerr << '\n' << "Error: " << ex.what() << '\n';
    return -1;
  }

  return 0;
}
