function tutorial05

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

ccalib_path = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', 'ccalib');
                
img_file1 = fullfile(ccalib_path, 'img_0000.png');
img_file2 = fullfile(ccalib_path, 'img_0001.png');
img_file3 = fullfile(ccalib_path, 'img_0002.png');

%%%

chess_det.name = 'CHESS_DETECTOR';
chess_det.parameters = struct;

ell_det.name = 'ELLIPSE_DETECTOR';
ell_det.parameters = struct;

dataset1.id = 'dts1';
dataset1.detectors = { chess_det };

dataset2.id = 'dts2';
dataset2.detectors = { chess_det, ell_det };

desc = { dataset1, dataset2 };

%%%

dts = MkeccDatasets();
dts.setDescription(desc);

dts.initialize();

im = imread(img_file1);
dts.processFrame(im, 'dts1');
dts.processImageFile(img_file2, 'dts1');
dts.processImageFile(img_file3, 'dts1');

dts.processImageFile(img_file1, 'dts2');
dts.processImageFile(img_file2, 'dts2');
dts.processImageFile(img_file3, 'dts2');

%%%

data = dts.getDatasets();
dts.show(data, 'dts1', 1);
dts.show(data, 'dts2', 1);
