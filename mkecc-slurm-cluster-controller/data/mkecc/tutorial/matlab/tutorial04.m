function tutorial04

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

ccalib_path = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', 'ccalib');
                
img_file1 = fullfile(ccalib_path, 'img_0000.png');
img_file2 = fullfile(ccalib_path, 'img_0001.png');
img_file3 = fullfile(ccalib_path, 'img_0002.png');

%%%

chess_det.name = 'CHESS_DETECTOR';
chess_det.parameters = struct;

dataset.id = 'dts1';
dataset.detectors = { chess_det };

desc = { dataset };

MkeccJsonFile.Dump(desc);

%%%

dts = MkeccDatasets();
dts.setDescription(desc);

dts.initialize();
dts.processImageFile(img_file1, 'dts1');
dts.processImageFile(img_file2, 'dts1');

im = imread(img_file3);
dts.processFrame(im, 'dts1');

%%%

data = dts.getDatasets();
dts.show(data, 'dts1', 1);
dts.show(data, 'dts1', 2);
dts.show(data, 'dts1', 3);

MkeccJsonFile.Dump(data);
