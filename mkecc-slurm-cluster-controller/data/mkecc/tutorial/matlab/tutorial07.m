function tutorial07

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

ccalib_path = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', 'ccalib');
                
%%%

chess_det.name = 'CHESS_DETECTOR';
chess_det.parameters = struct;

ell_det.name = 'ELLIPSE_DETECTOR';
ell_det.parameters = struct;

dataset1.id = 'dts1';
dataset1.detectors = { chess_det };

dataset2.id = 'dts2';
dataset2.detectors = { chess_det, ell_det };

desc = { dataset1, dataset2 };

%%%

list1.dataset = 'dts1';
list1.directory.path = ccalib_path;
list1.directory.mask = '.*';
list1.directory.order = 'lex';

list2.dataset = 'dts2';
list2.directory.path = ccalib_path;
list2.directory.mask = '.*';
list2.directory.order = 'lex';

image_list = { list1, list2 };

%%%

dts = MkeccDatasets();
dts.setDescription(desc);

dts.initialize();
dts.processImageList(image_list);

%%%

data = dts.getDatasets();
dts.show(data, 'dts1', 10);
dts.show(data, 'dts2', 10);
