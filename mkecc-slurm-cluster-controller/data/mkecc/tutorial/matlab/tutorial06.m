function tutorial06

fname = mfilename('fullpath');
[path, ~, ~] = fileparts(fname);

ccalib_path = fullfile(path, '..', '..', ...
                    'data', 'vcsel_example_01', 'ccalib');
                
%%%

chess_det.name = 'CHESS_DETECTOR';
chess_det.parameters = struct;

dataset1.id = 'dts1';
dataset1.detectors = { chess_det };

desc = { dataset1 };

%%%

list1.dataset = 'dts1';
list1.directory.path = ccalib_path;
list1.directory.mask = '.*';
list1.directory.order = 'lex';
image_list = { list1 };

%%%

dts = MkeccDatasets();
dts.setDescription(desc);

dts.initialize();
dts.processImageList(image_list);

%%%

data = dts.getDatasets();
dts.show(data, 'dts1', 1);
dts.show(data, 'dts1', 10);
