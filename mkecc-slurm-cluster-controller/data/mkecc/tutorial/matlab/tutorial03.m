function tutorial03
                
avail_dets = MkeccDetector.getAvailableDetectors();
disp('Available detectors:');
disp(avail_dets);

%%%                

chess_det = MkeccDetector('CHESS_DETECTOR');

meta_info = chess_det.getMetaInfo();
disp('CHESS_DETECTOR information:');
disp(meta_info);

params = chess_det.getParams();
disp('CHESS_DETECTOR parameters:');
disp(params);

param_schema = chess_det.getParamSchema();
disp('CHESS_DETECTOR parameter schema:');
MkeccJsonFile.Dump(param_schema); % nice JSON style output

%%%

ell_det = MkeccDetector('ELLIPSE_DETECTOR');

meta_info = ell_det.getMetaInfo();
disp('ELLIPSE_DETECTOR information');
disp(meta_info);

params = ell_det.getParams();
disp('ELLIPSE_DETECTOR parameters:');
disp(params);

params.delta = 22;
ell_det.setParams(params);

param_schema = ell_det.getParamSchema();
disp('CHESS_DETECTOR parameter schema:');
MkeccJsonFile.Dump(param_schema); % nice JSON style output
