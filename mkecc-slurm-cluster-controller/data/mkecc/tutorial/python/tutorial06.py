import pymkecc
import os
from PIL import Image
import matplotlib.pyplot as plt
import json

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')

###############################################################################

desc = [
    {
        'id': 'dts1',
        'detectors': [
            {
                'name': 'CHESS_DETECTOR',
                'parameters': {}
            }
        ]
    }
]

dts = pymkecc.Datasets()
dts.setDescription(desc)

###############################################################################

image_list = [
    {
        'dataset': 'dts1',
        'directory': {
            'path': ccalib_path,
            'mask': '.*',
            'order': 'lex',
        }
    }
]

dts.initialize()
dts.processImageList(image_list)

###############################################################################

data = dts.getDatasets()

for i in [0, 10]:
    img_path = data['dts1']['frames'][i]['uri'][7:]
    results = data['dts1']['detectors'][0]['results'][i]

    im = Image.open(img_path)
    plt.figure()
    plt.imshow(im)

    for p in results:
        plt.plot(p[0], p[1], 'ro')

plt.show()
