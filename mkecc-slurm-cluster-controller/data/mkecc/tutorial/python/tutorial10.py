import pymkecc
import json

###############################################################################

nodes = pymkecc.Scenario.getAvailableNodes()

print('Available scenario nodes:')
print(nodes)
print()

###############################################################################

schema = pymkecc.Scenario.getNodeTypeParamsSchema('CameraCalibration')

print('"CameraCalibration" parameter schema:')
print(json.dumps(schema, indent=2) + '\n')

###############################################################################

desc = [
    {
        'id': 'ccalib',
        'type': 'CameraCalibration',
        'parameters': {
            'camera_model_type': 'OPENCV_MODEL',
            'target_type': 'ELMARK_CHESSBOARD_TARGET'
        }
    }
]

scenario = pymkecc.Scenario()
scenario.setDescription(desc)

###############################################################################

dts_desc = scenario.getDatasetsDescription()
print('Scenario induced datasets description:')
print(json.dumps(dts_desc, indent=2) + '\n')
