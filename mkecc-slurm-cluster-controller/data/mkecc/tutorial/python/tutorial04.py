import pymkecc
import os
from PIL import Image
import numpy
import matplotlib.pyplot as plt
import json

###############################################################################

tutorial_dir = os.path.dirname(os.path.realpath(__file__))
ccalib_path = os.path.join(tutorial_dir, '..', '..', 'data', 'vcsel_example_01', 'ccalib')

img_file1 = os.path.join(ccalib_path, 'img_0000.png')
img_file2 = os.path.join(ccalib_path, 'img_0001.png')
img_file3 = os.path.join(ccalib_path, 'img_0002.png')

###############################################################################

desc = [
    {
        'id': 'dts1',
        'detectors': [
            {
                'name': 'CHESS_DETECTOR',
                'parameters': {}
            }
        ]
    }
]

print(json.dumps(desc, indent=2) + "\n")

dts = pymkecc.Datasets()
dts.setDescription(desc)

###############################################################################

dts.initialize()
dts.processImageFile(img_file1, 'dts1')
dts.processImageFile(img_file2, 'dts1')

im = Image.open(img_file3)
dts.processFrame(numpy.array(im), 'dts1')

data = dts.getDatasets()

###############################################################################

frames = data['dts1']['frames']
detectors = data['dts1']['detectors']

for i in range(len(frames)):
    frame = frames[i]
    image_path = frame['uri'][7:]

    plt.figure()

    if os.path.isfile(image_path):
        im = Image.open(image_path)
        plt.imshow(im)

    results = detectors[0]['results'][i]
    for p in results:
        plt.plot(p[0], p[1], 'ro')

plt.show()
